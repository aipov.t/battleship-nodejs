const readline = require('readline-sync');
const gameController = require("./GameController/gameController.js");
const cliColor = require('cli-color');
const beep = require('beepbeep');
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");

class Battleship {
    constructor() {
        this.enemyFleetPos = [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ];
    }

    start() {
        console.log(cliColor.magenta("                                     |__"));
        console.log(cliColor.magenta("                                     |\\/"));
        console.log(cliColor.magenta("                                     ---"));
        console.log(cliColor.magenta("                                     / | ["));
        console.log(cliColor.magenta("                              !      | |||"));
        console.log(cliColor.magenta("                            _/|     _/|-++'"));
        console.log(cliColor.magenta("                        +  +--|    |--|--|_ |-"));
        console.log(cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/"));
        console.log(cliColor.magenta("                    +---------------___[}-_===_.'____                 /\\"));
        console.log(cliColor.magenta("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"));
        console.log(cliColor.magenta(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"));
        console.log(cliColor.magenta("|                        Welcome to Battleship                         BB-61/"));
        console.log(cliColor.magenta(" \\_________________________________________________________________________|"));
        console.log();

        this.InitializeGame();
        this.StartGame();
    }

    StartGame() {
        console.clear();

        this.printFleet(this.enemyFleet);

        console.log(cliColor.cyan("                  __"));
        console.log(cliColor.cyan("                 /  \\"));
        console.log(cliColor.cyan("           .-.  |    |"));
        console.log(cliColor.cyan("   *    _.-'  \\  \\__/"));
        console.log(cliColor.cyan("    \\.-'       \\"));
        console.log(cliColor.cyan("   /          _/"));
        console.log(cliColor.cyan("  |      _  /"));
        console.log(cliColor.cyan("  |     /_\\'"));
        console.log(cliColor.cyan("   \\    \\_/"));
        console.log(cliColor.cyan("    \"\"\"\""));

        do {
            const aliveShips = {};
            const murderedShips = {};

            this.enemyFleet.forEach(ship => {
                if (ship.isMurdered()) {
                    if (typeof murderedShips[ship.size] === 'number') {
                        murderedShips[ship.size]++;
                    } else {
                        murderedShips[ship.size] = 1;
                    }
                } else {
                    if (typeof aliveShips[ship.size] === 'number') {
                        aliveShips[ship.size]++;
                    } else {
                        aliveShips[ship.size] = 1;
                    }
                }
            });

            let aliveOutput = "";

            for (const shipSize in aliveShips) {
                aliveOutput += `${shipSize}x${aliveShips[shipSize]} `;
            }

            let murderedShipsOutput = "";

            for (const shipSize in murderedShips) {
                murderedShipsOutput += `${shipSize}x${murderedShips[shipSize]} `;
            }

            console.log();
            console.log("Player, it's your turn");
            console.log("-----------");
            console.log("Enemy battleships which are alive: ", aliveOutput === "" ? cliColor.blue("none") : cliColor.green(aliveOutput));
            console.log("Enemy battleships which are murdered: ", murderedShipsOutput === "" ? cliColor.blue("none") : cliColor.red(murderedShipsOutput));
            console.log("-----------");
            console.log("Enter coordinates for your shot :");
            var position = Battleship.ParsePosition(readline.question());

            if(position.column == undefined || position.row < 1 || position.row > 8 || !position.row) {
                    console.log(cliColor.red("Invalid position, please try again"));
                 continue;
            }
            var isHit = gameController.CheckIsHit(this.enemyFleet, position);
            if (isHit) {
                beep();

                console.log(cliColor.red("                \\         .  ./"));
                console.log(cliColor.red("              \\      .:\";'.:..\"   /"));
                console.log(cliColor.red("                  (M^^.^~~:.'\")."));
                console.log(cliColor.red("            -   (/  .    . . \\ \\)  -"));
                console.log(cliColor.red("               ((| :. ~ ^  :. .|))"));
                console.log(cliColor.red("            -   (\\- |  \\ /  |  /)  -"));
                console.log(cliColor.red("                 -\\  \\     /  /-"));
                console.log(cliColor.red("                   \\  \\   /  /"));
            } else {
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
            }

            console.log(isHit ? "Yeah ! Nice hit !" : "Miss");

            var computerPos = this.GetRandomPosition();
            var isHit = gameController.CheckIsHit(this.myFleet, computerPos);
            console.log();
            console.log(`Computer shot in ${computerPos.column}${computerPos.row} and ` + (isHit ? `has hit your ship !` : `miss`));
            if (isHit) {
                beep();

                console.log(cliColor.red("                \\         .  ./"));
                console.log(cliColor.red("              \\      .:\";'.:..\"   /"));
                console.log(cliColor.red("                  (M^^.^~~:.'\")."));
                console.log(cliColor.red("            -   (/  .    . . \\ \\)  -"));
                console.log(cliColor.red("               ((| :. ~ ^  :. .|))"));
                console.log(cliColor.red("            -   (\\- |  \\ /  |  /)  -"));
                console.log(cliColor.red("                 -\\  \\     /  /-"));
                console.log(cliColor.red("                   \\  \\   /  /"));
            } else {
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
                console.log(cliColor.blue('~~~~~~~~~~~~~~~'));
            }
        }
        while (true);
    }

    static ParsePosition(input) {
        var letter = letters.get(input.toUpperCase().substring(0, 1));
        var number = parseInt(input.substring(1, 2), 10);
        return new position(letter, number);
    }

    GetRandomPosition() {
        var rows = 8;
        var lines = 8;
        var rndColumn = Math.floor((Math.random() * lines));
        var letter = letters.get(rndColumn + 1);
        var number = Math.floor((Math.random() * rows));
        var result = new position(letter, number);
        return result;
    }

    InitializeGame() {
        this.InitializeMyFleet();
        this.InitializeEnemyFleet();
    }

    InitializeMyFleet() {
        this.myFleet = gameController.InitializeShips();

        console.log("Please position your fleet (Game board size is from A to H and 1 to 8) :");

        this.myFleet.forEach(function (ship) {
            console.log();
            console.log(`Please enter the positions for the ${ship.name} (size: ${ship.size})`);
            for (var i = 1; i < ship.size + 1; i++) {
                console.log(`Enter position ${i} of ${ship.size} (i.e A3):`);
                const position = readline.question();
                ship.addPosition(Battleship.ParsePosition(position));
            }
        })
    }

    InitializeEnemyFleet() {
        const rand = this.getRandomInt(0, 4);

        this.enemyFleet = gameController.InitializeShips();

        const columnMap = {
            [letters.A]: 0,
            [letters.B]: 1,
            [letters.C]: 2,
            [letters.D]: 3,
            [letters.E]: 4,
            [letters.F]: 5,
            [letters.G]: 6,
            [letters.H]: 7,
        };

        switch (rand) {
            case 0:
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleetPos[columnMap[letters.B]][4] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleetPos[columnMap[letters.B]][5] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleetPos[columnMap[letters.B]][6] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 7));
                this.enemyFleetPos[columnMap[letters.B]][7] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 8));
                this.enemyFleetPos[columnMap[letters.B]][8] = 1;

                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleetPos[columnMap[letters.E]][6] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleetPos[columnMap[letters.E]][7] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 8));
                this.enemyFleetPos[columnMap[letters.E]][8] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 9));
                this.enemyFleetPos[columnMap[letters.E]][9] = 1;

                this.enemyFleet[2].addPosition(new position(letters.A, 3));
                this.enemyFleetPos[columnMap[letters.A]][3] = 1;
                this.enemyFleet[2].addPosition(new position(letters.B, 3));
                this.enemyFleetPos[columnMap[letters.B]][3] = 1;
                this.enemyFleet[2].addPosition(new position(letters.C, 3));
                this.enemyFleetPos[columnMap[letters.C]][3] = 1;

                this.enemyFleet[3].addPosition(new position(letters.F, 8));
                this.enemyFleetPos[columnMap[letters.F]][8] = 1;
                this.enemyFleet[3].addPosition(new position(letters.G, 8));
                this.enemyFleetPos[columnMap[letters.G]][8] = 1;
                this.enemyFleet[3].addPosition(new position(letters.H, 8));
                this.enemyFleetPos[columnMap[letters.H]][8] = 1;

                this.enemyFleet[4].addPosition(new position(letters.C, 5));
                this.enemyFleetPos[columnMap[letters.C]][5] = 1;
                this.enemyFleet[4].addPosition(new position(letters.C, 6));
                this.enemyFleetPos[columnMap[letters.C]][6] = 1;

                return;
            case 1:
                this.enemyFleet[0].addPosition(new position(letters.B, 3));
                this.enemyFleetPos[columnMap[letters.B]][3] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleetPos[columnMap[letters.B]][4] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleetPos[columnMap[letters.B]][5] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleetPos[columnMap[letters.B]][6] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 7));
                this.enemyFleetPos[columnMap[letters.B]][7] = 1;

                this.enemyFleet[1].addPosition(new position(letters.E, 5));
                this.enemyFleetPos[columnMap[letters.E]][5] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleetPos[columnMap[letters.E]][6] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleetPos[columnMap[letters.E]][7] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 8));
                this.enemyFleetPos[columnMap[letters.E]][8] = 1;

                this.enemyFleet[2].addPosition(new position(letters.A, 2));
                this.enemyFleetPos[columnMap[letters.A]][2] = 1;
                this.enemyFleet[2].addPosition(new position(letters.B, 2));
                this.enemyFleetPos[columnMap[letters.B]][2] = 1;
                this.enemyFleet[2].addPosition(new position(letters.C, 2));
                this.enemyFleetPos[columnMap[letters.C]][2] = 1;

                this.enemyFleet[3].addPosition(new position(letters.F, 7));
                this.enemyFleetPos[columnMap[letters.F]][7] = 1;
                this.enemyFleet[3].addPosition(new position(letters.G, 7));
                this.enemyFleetPos[columnMap[letters.G]][7] = 1;
                this.enemyFleet[3].addPosition(new position(letters.H, 7));
                this.enemyFleetPos[columnMap[letters.H]][7] = 1;

                this.enemyFleet[4].addPosition(new position(letters.C, 4));
                this.enemyFleetPos[columnMap[letters.C]][4] = 1;
                this.enemyFleet[4].addPosition(new position(letters.C, 5));
                this.enemyFleetPos[columnMap[letters.C]][5] = 1;

                return;
            case 2:
                this.enemyFleet[0].addPosition(new position(letters.B, 2));
                this.enemyFleetPos[columnMap[letters.B]][2] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 3));
                this.enemyFleetPos[columnMap[letters.B]][3] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleetPos[columnMap[letters.B]][4] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleetPos[columnMap[letters.B]][5] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleetPos[columnMap[letters.B]][6] = 1;

                this.enemyFleet[1].addPosition(new position(letters.E, 4));
                this.enemyFleetPos[columnMap[letters.E]][4] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 5));
                this.enemyFleetPos[columnMap[letters.E]][5] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleetPos[columnMap[letters.E]][6] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleetPos[columnMap[letters.E]][7] = 1;

                this.enemyFleet[2].addPosition(new position(letters.A, 1));
                this.enemyFleetPos[columnMap[letters.A]][1] = 1;
                this.enemyFleet[2].addPosition(new position(letters.B, 1));
                this.enemyFleetPos[columnMap[letters.B]][1] = 1;
                this.enemyFleet[2].addPosition(new position(letters.C, 1));
                this.enemyFleetPos[columnMap[letters.C]][1] = 1;

                this.enemyFleet[3].addPosition(new position(letters.F, 6));
                this.enemyFleetPos[columnMap[letters.F]][6] = 1;
                this.enemyFleet[3].addPosition(new position(letters.G, 6));
                this.enemyFleetPos[columnMap[letters.G]][6] = 1;
                this.enemyFleet[3].addPosition(new position(letters.H, 6));
                this.enemyFleetPos[columnMap[letters.H]][6] = 1;

                this.enemyFleet[4].addPosition(new position(letters.C, 3));
                this.enemyFleetPos[columnMap[letters.C]][3] = 1;
                this.enemyFleet[4].addPosition(new position(letters.C, 4));
                this.enemyFleetPos[columnMap[letters.C]][4] = 1;

                return;
            case 3:
                this.enemyFleet[0].addPosition(new position(letters.B, 1));
                this.enemyFleetPos[columnMap[letters.B]][1] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 2));
                this.enemyFleetPos[columnMap[letters.B]][2] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 3));
                this.enemyFleetPos[columnMap[letters.B]][3] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleetPos[columnMap[letters.B]][4] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleetPos[columnMap[letters.B]][5] = 1;

                this.enemyFleet[1].addPosition(new position(letters.E, 3));
                this.enemyFleetPos[columnMap[letters.E]][3] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 4));
                this.enemyFleetPos[columnMap[letters.E]][4] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 5));
                this.enemyFleetPos[columnMap[letters.E]][5] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleetPos[columnMap[letters.E]][6] = 1;

                this.enemyFleet[2].addPosition(new position(letters.A, 8));
                this.enemyFleetPos[columnMap[letters.A]][8] = 1;
                this.enemyFleet[2].addPosition(new position(letters.B, 8));
                this.enemyFleetPos[columnMap[letters.B]][8] = 1;
                this.enemyFleet[2].addPosition(new position(letters.C, 8));
                this.enemyFleetPos[columnMap[letters.C]][8] = 1;

                this.enemyFleet[3].addPosition(new position(letters.F, 5));
                this.enemyFleetPos[columnMap[letters.F]][5] = 1;
                this.enemyFleet[3].addPosition(new position(letters.G, 5));
                this.enemyFleetPos[columnMap[letters.G]][5] = 1;
                this.enemyFleet[3].addPosition(new position(letters.H, 5));
                this.enemyFleetPos[columnMap[letters.H]][5] = 1;

                this.enemyFleet[4].addPosition(new position(letters.C, 2));
                this.enemyFleetPos[columnMap[letters.C]][2] = 1;
                this.enemyFleet[4].addPosition(new position(letters.C, 3));
                this.enemyFleetPos[columnMap[letters.C]][3] = 1;

                return;
            case 4:
                this.enemyFleet[0].addPosition(new position(letters.B, 2));
                this.enemyFleetPos[columnMap[letters.B]][2] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 3));
                this.enemyFleetPos[columnMap[letters.B]][3] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleetPos[columnMap[letters.B]][4] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleetPos[columnMap[letters.B]][5] = 1;
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleetPos[columnMap[letters.B]][6] = 1;

                this.enemyFleet[1].addPosition(new position(letters.E, 2));
                this.enemyFleetPos[columnMap[letters.E]][2] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 3));
                this.enemyFleetPos[columnMap[letters.E]][3] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 4));
                this.enemyFleetPos[columnMap[letters.E]][4] = 1;
                this.enemyFleet[1].addPosition(new position(letters.E, 5));
                this.enemyFleetPos[columnMap[letters.E]][5] = 1;

                this.enemyFleet[2].addPosition(new position(letters.A, 7));
                this.enemyFleetPos[columnMap[letters.A]][7] = 1;
                this.enemyFleet[2].addPosition(new position(letters.B, 7));
                this.enemyFleetPos[columnMap[letters.B]][7] = 1;
                this.enemyFleet[2].addPosition(new position(letters.C, 7));
                this.enemyFleetPos[columnMap[letters.C]][7] = 1;

                this.enemyFleet[3].addPosition(new position(letters.F, 4));
                this.enemyFleetPos[columnMap[letters.F]][4] = 1;
                this.enemyFleet[3].addPosition(new position(letters.G, 4));
                this.enemyFleetPos[columnMap[letters.G]][4] = 1;
                this.enemyFleet[3].addPosition(new position(letters.H, 4));
                this.enemyFleetPos[columnMap[letters.H]][4] = 1;

                this.enemyFleet[4].addPosition(new position(letters.C, 1));
                this.enemyFleetPos[columnMap[letters.C]][1] = 1;
                this.enemyFleet[4].addPosition(new position(letters.C, 2));
                this.enemyFleetPos[columnMap[letters.C]][2] = 1;

                return;
        }
    }

    printFleet() {
        let fleetString = "";

        for (let i = 0; i < 8; i++) {
            for (let z = 0; z < 8; z++) {
                fleetString += this.enemyFleetPos[i][z] + " ";
            }

            fleetString += "\n";
        }

        console.log(cliColor.red(fleetString));
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

module.exports = Battleship;