class Ship {
    constructor(name, size, color) {
        this.name = name;
        this.size = size;
        this.color = color;
        this.positions = [];
    }

    addPosition(position) {
        this.positions.push(position);
    }

    isMurdered() {
        for(let i = 0; i < this.positions.length; i++) {
            if (!this.positions[i].isHit) {
                return false;
            }
        }

        return true;
    }
}

module.exports = Ship;