class Position {
    constructor(column, row) {
        this.column = column;
        this.row = row;
        this.isHit = false;
    }
}

module.exports = Position;